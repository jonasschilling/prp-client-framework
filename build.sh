#! /bin/bash

# PRP Project
# Copyright (C) 2020 The PRP Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

jsbuild='java -jar ./lib/closure.jar --js_output_file ./scripts.js --compilation_level SIMPLE '

jsbuild=$jsbuild'--js ./sjs/localstoragemanager.sjs '
jsbuild=$jsbuild'--js ./sjs/logger.sjs '
jsbuild=$jsbuild'--js ./sjs/pluginmanager.sjs '
jsbuild=$jsbuild'--js ./sjs/router.sjs '

for f in `find ./sjs -iname '*.service.sjs'`
do
jsbuild=$jsbuild'--js '$f' '
done

for f in `find ./sjs -iname '*.controller.sjs'`
do
jsbuild=$jsbuild'--js '$f' '
done

for f in `find ./sjs -iname '*.content.sjs'`
do
jsbuild=$jsbuild'--js '$f' '
done

for f in `find ./sjs -iname '*.view.sjs'`
do
jsbuild=$jsbuild'--js '$f' '
done

jsbuild=$jsbuild'--js ./sjs/init.sjs '

cd plugins

for plugin in `ls -d */`
do
jsbuild=$jsbuild'--js ./plugins/'$plugin'init.sjs '
done

cd ..

echo "Running Closure build command: "$jsbuild

eval $jsbuild

sass scss/styles.scss styles.css --style compressed