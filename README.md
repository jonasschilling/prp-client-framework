# PRP Project #

Welcome to the PRP project! We're developing the first system, that gives you time back.

## Maintainers ##

The project is maintained by [Eric Fischer](mailto:prp@ericfischer.eu) and [Felix Schiessl](mailto:prp@felixschiessl.de) and many volunteer contributors.

## More Information ##

You can find more details about the project in our [developer resources repository](https://bitbucket.org/personalresourceplanning/prp-developer-resources/src).

# Client Application Framework #

The Client Application Framework is the wrapper for all PRP applications. It provides a plugin functionality to build different apps on the same basis.

# Contributing #

Please read the guidelines in our [developer resources repository](https://bitbucket.org/personalresourceplanning/prp-developer-resources/src) and the CLA.md in this repository.
