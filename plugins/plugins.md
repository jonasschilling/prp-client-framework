# PRP Client Plugin Design #

In order to be able to plug in different sources together into one platform, the PRP system contains a client plugin system. This document outlines the structures of this system and how to integrate plugins.

## Structure ##

Every plugin should be built on the following structure:

```
plugin-wrapper.sjs
sjs/
scss/
templates/
lib/
resources/
translations/
```

All components will be explained within the following chapters.

## Source Files And Build ##

All files are build together with the platform into one single application. It doesn't matter if you're integrating just one or many plugins. This is the reason, why all source JavaScript files have to have the ending `.sjs`. Furthermore, CSS is build with the SASS compiler, but written in SCSS to be compatible with the classic CSS.

With building all the files, neither your JavaScript nor your CSS have to be included into the main application. Just make the structure, the build does the rest.

## The plugin-wrapper.sjs ##

This file initializes your plugin. First of all, you have to define a namespace for your plugin. This is done by registering your plugin to the plugin manager:

```
Pluginmanager.register("myplugin", function() {

  var name = "My Plugin";

  return {
    name: name
  };

});
```

Plugins have to follow the JavaScript module pattern. So the function you hand to the plugin manager can contain other functions, even private functions:

```
Pluginmanager.register("myplugin", function() {
  
  // private function
  var doIt = function() {
    console.log("I'm done.");
  };
  
  // public function
  this.publicFunction = function() {
    console.log("Yes, you can call me from everywhere!");
  };

});
```

To call the functions you define during the plugin registration, just call `Pluginmanager.get("myplugin").publicFunction()`

### Overview Module ###

On the overview page, plugins can generate an own module, showing the most important content shortly. Your `plugin-wrapper.sjs` has to provide a `name` and an `iconPath`. Both habe to be accessable publically. The `iconPath` has to point to a resource in your plugin folder, which must have the same name as your registered ID. E.g., you could point to an image in your `resources` folder. You should place a SVG here, as this can be tinted as needed.

Furthermore, your plugin must implement `generateOverviewModule(container)`. The container will be a string value representing the HTML container for your module, where you can place your content in. Every plugin registered with the `Pluginmanager` get it's own module. In future, the user will be able to sort the modules by himself.

### Loading Pages ###

The plugin has also to provide a function `openPage(url)`, where `url` is a string containing the path to the requested page. The absolute path showing in the address field of your browser will start with your plugin name, followed by the page's path. The URL handed to your plugin will only contain the path to your page. So when you see `?page=myplugin/page/subpage` in your browser, you'll only get `page/subpage` as URL.

### Full Required Outline ###

```
Pluginmanager.register("myplugin", function() {

  this.name = "My Plugin";
  this.iconPath = "resources/logo.svg";
  
  this.generateOverviewModule = function(container) {
    // generate your overview module content
  }
  
  this.openPage = function(url) {
    switch(url) {
      case "page":
        // open your page (path just an example)
      break;
      case "page/subpage":
        // open your subpage (path just an example)
      break;
      default:
        // if you want to, you can implement a "Not Found" section here
      break;
    }
  }

});
```

## Other Source Files ##

Of course, you don't want to write your whole code into the `plugin-wrapper.sjs`. You can place other source files into your `sjs` directory. Every `.sjs` file there will be built together with the wrapper file. To ensure, that your source files belong into your namespace, initialize every class with

```
Pluginmanager.get("MyPlugin").MyView = function() {
  // define your view here
}
```

So you can instanciate your view at every place with `var view = new Pluginmanager.get("MyPlugin").MyView();`

Hint: You can shorten your code by holding the plugin in a variable: 

```
var myPlugin = Pluginmanager.get("MyPlugin");
var view = new myPlugin.MyView();
```
