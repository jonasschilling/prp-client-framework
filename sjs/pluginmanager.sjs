/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class Pluginmanager {

    constructor() {
        this.plugins = new Map();
        this.notifications = [];
    }

    register(name, pluginClass) {
        // Create instance from pluginClass
        let pluginObject = new pluginClass();
        this.plugins.set(name, pluginObject);

        // create home route to the plugin
        addRoute(pluginObject.homeUrl, pluginObject.name);

        // Create Main Nav Menu
        this.createNavMenu(pluginObject);

        //Create Sub menus
        this.createSubNavMenus(pluginObject);
    }

    createNavMenu(plugin) {
        let mainNavBarTemplate = document.getElementById('mainNavBarItem').content;
        let templateCopy = document.importNode(mainNavBarTemplate, true);

        templateCopy.querySelector('.mainNavBarItemImg').setAttribute('src', plugin.iconPath);
        templateCopy.querySelector('.mainNavBarItemPath').setAttribute('href', '#' + plugin.homeUrl);
        templateCopy.querySelector('.mainNavBarItemTitle').textContent = plugin.name;

        document.getElementById('side').appendChild(templateCopy);
    }

    createSubNavMenus(plugin) {
        if (plugin.subRoutes !== undefined) {
            plugin.subRoutes.forEach(subRoute => {
                // register sub route
                addRoute(subRoute.subPath, subRoute.menuName);

                // create sub Nav icons
                let sideNavBarTemplate = document.getElementById('sideNavBarItem').content;
                let templateCopy = document.importNode(sideNavBarTemplate, true);

                templateCopy.querySelector('.sideNavBarItemImg').setAttribute('src', subRoute.iconPath);
                templateCopy.querySelector('.sideNavBarItemPath').setAttribute('href', '#' + subRoute.subPath);
                templateCopy.querySelector('.sideNavBarItemTitle').textContent = subRoute.menuName;

                document.getElementById('side').appendChild(templateCopy);
            });
        }
    }

    addNotification(title, text, icon, color, link, priority) {
        let newNotification = {
            title: title,
            text: text,
            icon: icon,
            color: color,
            link: link,
            priority: priority
        };
        this.notifications.push(newNotification);
    }

    getNotifications() {
        return this.notifications;
    }

    getPlugins() {
        return this.plugins;
    }

}
