/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const ErrorComponent = {
    render: () => {
        return `
      <section>
        <h1>Error</h1>
        <p>This is just a test</p>
      </section>
    `;
    }
};

function findComponentByName(array, key, value) {
    for (let i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
}

// Routes
const Routes = [];

function addRoute(path, pluginName) {
    let component = {
        render: () => {
            $('#content').load('./plugins/' + pluginName.toLowerCase() + '/' + pluginName.toLowerCase() + '.html');
        }
    };

    Routes.push({
        path: path,
        component: component
    });
}

const router = () => {
    // Find the component based on the current path
    const path = parseLocation();

    // If there's no matching route, get the "Error" component
    const {component = ErrorComponent} = findComponentByPath(path, Routes) || {};

    // Render the component in the "app" placeholder
    document.getElementById('content').innerHTML = component.render();
};

const parseLocation = () => location.hash.slice(1).toLowerCase() || '/';
const findComponentByPath = (path, routes) => routes.find(r => r.path.match(new RegExp(`^\\${path}$`, 'gm'))) || undefined;

window.addEventListener('hashchange', router);
window.addEventListener('load', router);
